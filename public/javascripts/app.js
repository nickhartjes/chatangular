'use strict';


// Declare app level module which depends on filters, and services
var app = angular.module('myApp', [
	'btford.socket-io',
	'myApp.filters', 
	'myApp.directives']
);
