/**
 * Module dependencies.
 */

var mongoose = require('mongoose');
/**
 * MessageSchema Schema
 */

MessageSchema = new mongoose.Schema({
	  to: { type: String, default: '' },
	  from: { type: String, default: '' },
	  message: { type: String, default: '' },
	  timeSend: { type: Date, default: Date.now },
	  timeRead: { type: Date, default: '' }
	})

/**
 * Validation
 */

/*
MessageSchema.path('to').validate(function (to) {
  // if you are authenticating by any of the oauth strategies, don't validate
  if (authTypes.indexOf(this.provider) !== -1) return true
  return to.length
}, 'Receiver cannot be empty')

MessageSchema.path('from').validate(function (from) {
  // if you are authenticating by any of the oauth strategies, don't validate
  if (authTypes.indexOf(this.provider) !== -1) return true
  return message.length
}, 'Sender cannot be empty')

MessageSchema.path('message').validate(function (message) {
  if (authTypes.indexOf(this.provider) !== -1) return true
  return message.length
}, 'Message cannot be empty')
*/


/**
 * Methods
 */
MessageSchema.methods = {
	say: function (plainText) {
		console.log(plainText)
	}
}

var Message = mongoose.model('Message', MessageSchema)

module.exports = {
	Message: Message
}
